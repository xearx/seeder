const { promisify } = require('util');

/**
 * Checks whether array is empty or not.
 * returns `array.length === 0`.
 * @param {any[]} array
 */
function isEmpty(array) {
  return array.length === 0;
}

const delay = promisify(setTimeout);

module.exports = {
  isEmpty,
  delay,
};
