const logger = require('../logger');
const { EVENT } = require('../constants');
const config = require('../config');
const filterer = require('../grpc/clients/filterer');

const Seeder = {
  async init({ messenger, repository, bus }) {
    this.bus = bus;
    this.messenger = messenger;
    this.batchGenerator = repository.urlsBatchGenerator(config.repository.batchSize);
  },
  emitAddUrls(urls) {
    this.bus.emit(EVENT.URLS_CRAWLED, urls);
  },
  async handleMessageReceived(urlsAddedEvent) {
    this.emitAddUrls(urlsAddedEvent);
    await this.fetchBatchAndSendToCrawlers();
  },
  async fetchNextBatch() {
    logger.trace('calling batch generator to generate batch of urls...');
    const { value: batch } = await this.batchGenerator.next();
    logger.trace({ batch_length: batch.length }, 'batch received from repository.');
    return batch;
  },
  async fetchBatchAndSendToCrawlers() {
    const batch = await this.fetchNextBatch();
    const filteredBatch = await this.filterBatchBeforeSend(batch);
    await this.sendUrlsToCrawlers(filteredBatch);
  },
  filterBatchBeforeSend(batch) {
    return filterer.filterUrls(batch);
  },
  async handleCrawlerAdd(crawlerId) {
    logger.trace({ crawlerId }, 'receiving crawler registration event.');
    await this.fetchBatchAndSendToCrawlers();
  },
  async sendUrlsToCrawlers(urls) {
    logger.trace('sending batch to crawler queue...');
    await this.messenger.sendUrlsToCrawler(urls);
    logger.trace('batch sent to crawler queue...');
  },
};

const listeners = {
  [EVENT.MESSAGE_RECEIVED]: Seeder.handleMessageReceived.bind(Seeder),
  [EVENT.CRAWLER_REGISTERED]: Seeder.handleCrawlerAdd.bind(Seeder),
};

Seeder.registerListeners = function registerListeners(bus) {
  for (const [event, handler] of Object.entries(listeners)) {
    bus.addListener(event, handler);
  }
};

module.exports = Seeder;
