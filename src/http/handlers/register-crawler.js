const STATUS_CODES = require('http-status-codes');
const bus = require('../../bus');
const { EVENT } = require('../../constants');

/**
 * @param {import('fastify').FastifyRequest} request
 * @param {import('fastify').FastifyReply} reply
 */
module.exports = async function registerCrawlerHandler(request, reply) {
  const { crawler_id: crawlerId } = request.query;
  bus.emit(EVENT.CRAWLER_REGISTERED, crawlerId);
  reply.code(STATUS_CODES.CREATED).send({});
};
