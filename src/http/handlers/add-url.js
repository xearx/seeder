const STATUS_CODES = require('http-status-codes');
const bus = require('../../bus');
const { EVENT } = require('../../constants');

/**
 * @param {import('fastify').FastifyRequest} request
 * @param {import('fastify').FastifyReply} reply
 */
module.exports = async function addUrlHandler(request, reply) {
  const { urls } = request.body;
  bus.emit(EVENT.URLS_ADDED, urls);
  reply.code(STATUS_CODES.CREATED).send({});
};
