require('make-promises-safe');
const Fastify = require('fastify');
const addUrlHandler = require('./handlers/add-url');
const registerCrawlerHandler = require('./handlers/register-crawler');
const logger = require('../logger');

const fastify = Fastify({
  logger,
});

fastify.route({
  method: 'POST',
  url: '/api/v1/urls',
  handler: addUrlHandler,
});

fastify.route({
  method: 'POST',
  url: '/api/v1/crawlers/:crawler_id',
  handler: registerCrawlerHandler,
});

async function startServer() {
  try {
    await fastify.listen(3000);
  } catch (err) {
    fastify.log.error(err);
    process.exit(1);
  }
}

async function stopServer() {
  try {
    fastify.log.info('closing server...');
    await fastify.close();
    fastify.log.info('server closed.');
  } catch (err) {
    fastify.log.error(err);
    process.exit(1);
  }
}

module.exports = {
  startServer,
  stopServer,
};
