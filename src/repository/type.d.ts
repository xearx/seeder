export interface Event {
  type: string
  payload: any
}

export type Events = Event[];