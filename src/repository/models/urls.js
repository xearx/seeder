const { Schema, model } = require('mongoose');
const { DateTime } = require('luxon');
const logger = require('../../logger');

const UrlSchema = new Schema(
  {
    url: {
      type: String,
      required: true,
      unique: true,
    },
    date_added: {
      type: String,
      default: DateTime.utc().toISO(),
    },
  },
  {
    validateBeforeSave: false,
  },
);

const Urls = model('Url', UrlSchema, 'urls');

/**
 * @param {string[]} urls
 */
async function addMany(urls) {
  try {
    logger.trace({ length: urls.length }, 'adding urls...');
    await Urls.insertMany(
      urls.map((url) => ({
        url,
      })),
      {
        ordered: false,
      },
    );
    logger.trace('urls added.');
  } catch (error) {
    if (error.code === 11000) {
      return;
    }
    logger.error(error, 'an error occurred while adding urls.');
    console.error({ error });
  }
}

function getMany(offset, limit) {
  return Urls.find({}, ['url']).limit(limit).skip(offset);
}

function estimatedCount() {
  return Urls.estimatedDocumentCount();
}


module.exports = {
  Urls,
  addMany,
  getMany,
  estimatedCount,
};
