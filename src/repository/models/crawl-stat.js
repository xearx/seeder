const { Schema, model } = require('mongoose');
const logger = require('../../logger');

const CrawlStatSchema = new Schema(
  {
    last_offset: {
      type: Number,
      default: 0,
    },
  },
  {
    validateBeforeSave: false,
  },
);

const CrawlStats = model('CrawlStat', CrawlStatSchema, 'crawl_stats');

async function ensureStat() {
  try {
    logger.trace('asserting stats...');
    const firstStatItem = await CrawlStats.findOne({}, ['_id']);
    if (firstStatItem === null) {
      logger.trace('stat item was not found. creating it...');
      const statItem = new CrawlStats({});
      await CrawlStats.create(statItem);
      logger.trace('stat item created.');
    }
  } catch (error) {
    logger.error(error, 'an error occurred while ensuring crawl stat entity.');
  }
}

function getThenIncreaseOffset(increment) {
  return CrawlStats
    .findOneAndUpdate(
      {},
      { $inc: { last_offset: increment } },
      { new: false, lean: true, rawResult: true },
    )
    .select(['last_offset'])
    .exec()
    .then((result) => result.value.last_offset);
}

async function resetOffset() {
  logger.trace('resetting last_offset field of CrawlStat collection...');
  await CrawlStats.updateOne({}, { last_offset: 0 });
  logger.trace('last_offset field of CrawlStat collection was reset.');
}

module.exports = {
  CrawlStats,
  ensureStat,
  getThenIncreaseOffset,
  resetOffset,
};
