const logger = require('../logger');
const { EVENT } = require('../constants');
const urls = require('./models/urls');
const crawlStat = require('./models/crawl-stat');


async function* urlsBatchGenerator(limit = 100) {
  while (true) {
    try {
      const offset = await crawlStat.getThenIncreaseOffset(limit);
      logger.trace({ offset, limit }, 'fetching url batch...');
      const batch = await urls.getMany(offset, limit);
      logger.trace({ length: batch.length }, 'batch fetched.');
      logger.trace('yeilding batch fetched.');
      yield batch.map((doc) => doc.url);
      logger.trace('batch yielded.');
      const collectionSize = await urls.estimatedCount();
      logger.trace('checking whether offset needs to be updated for the next time...');
      if (offset + limit > collectionSize) {
        logger.trace({
          limit,
          collectionSize,
          offset,
        }, 'it seems it reached to the end of collection.');
        logger.trace('resetting offset...');
        await crawlStat.resetOffset();
        logger.trace('offset reset.');
      }
    } catch (error) {
      logger.error(error, 'an error occurred while fetching next url batch.');
      return [];
    }
  }
}

/**
 * @param {import('./type').Event} event
 */
async function applyEvent(event) {
  switch (event.type) {
    case EVENT.URLS_ADDED:
      await urls.addMany(event.payload);
      break;

    case EVENT.URLS_CRAWLED:
      // TODO: deal with it 😏
      break;

    default:
      throw new Error('how did you reach here?!');
  }
}

/**
 * @param {import('./type').Events} events
 */
async function applyEvents(events) {
  events.forEach(applyEvent);
}

const listeners = {
  [EVENT.URLS_ADDED]: function onUrlsAdded(addedUrls) {
    urls.addMany(addedUrls);
  },
};

function registerListeners(bus) {
  for (const [event, handler] of Object.entries(listeners)) {
    bus.addListener(event, handler);
  }
}

module.exports = {
  registerListeners,
  ensureStat: crawlStat.ensureStat,
  urlsBatchGenerator,
  applyEvents,
};
