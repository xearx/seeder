const mongoose = require('mongoose');
const config = require('../config');
const logger = require('../logger');

// https://mongoosejs.com/docs/connections.html#buffering
mongoose.set('bufferCommands', false);
mongoose.set('useFindAndModify', false);

async function connect() {
  const opts = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    serverSelectionTimeoutMS: 5000,
  };

  if (config.database.useAuth) {
    opts.user = config.database.username;
    opts.pass = config.database.password;
  }

  const connectionUri = `${config.database.host}:${config.database.port}/${config.database.name}`;

  try {
    logger.info(`connecting to database at: ${connectionUri}`);
    await mongoose.connect(`mongodb://${connectionUri}`, opts);
    logger.info('database successfully connected.');
  } catch (error) {
    logger.error('unable to connect to database.', error);
    process.exit(1);
  }
}

async function disconnect() {
  logger.info('closing database connection...');
  await mongoose.disconnect();
  logger.info('database connection closed.');
}

module.exports = {
  connect,
  disconnect,
};
