const amqp = require('amqplib');
const logger = require('../logger');
const config = require('../config');
const uuid = require('../uuid');
const { EVENT } = require('../constants');

const queues = {
  send: config.messaging.rabbitmq.queues.send,
  receive: config.messaging.rabbitmq.queues.receive,
};

const { host, port } = config.messaging.rabbitmq;

async function connect() {
  logger.trace({ host, port }, 'connecting to rabbitmq:');
  const connectionUrl = `amqp://${host}:${port}`;
  try {
    const connection = await amqp.connect(connectionUrl);
    logger.info('connection successfully established.');
    return connection;
  } catch (error) {
    logger.info(error, 'could not make connection.');
    throw new Error();
  }
}

async function createChannel() {
  logger.info('creating channel...');
  const channel = await this.connection.createChannel();
  logger.info('channel successfully created.');
  logger.info('asserting send channel...');
  await channel.assertQueue(queues.send.name, { durable: queues.send.durable });
  logger.info('send channel successfully asserted.');
  return channel;
}

async function registerConsumer() {
  logger.info('asserting receive channel...');
  await this.channel.assertQueue(queues.receive.name, { durable: queues.receive.durable });
  logger.info('receive channel successfully asserted.');
  const { consumerTag } = await this.channel.consume(queues.receive.name, async (message) => {
    await this.channel.ack(message);
    logger.trace('message received from crawlers.');
    const messageContent = JSON.parse(message.content);
    logger.trace('emitting message reception event to bus...');
    this.bus.emit(EVENT.MESSAGE_RECEIVED, messageContent);
    logger.trace('event emitted.');
  });
  logger.info('consumer successfully registered. tag: ', consumerTag);
  return consumerTag;
}

const Messenger = {
  async init({ bus }) {
    this.bus = bus;
    this.connection = await connect();
    this.channel = await createChannel.call(this);
    this.consumerTag = await registerConsumer.call(this);
  },
  async disconnect() {
    logger.info('closing messenger connection...');
    await this.cancelConsumption();
    await this.connection?.close();
    logger.info('messenger connection successfully closed.');
  },
  async cancelConsumption() {
    logger.info('canceling messenger consumption...');
    await this.channel?.cancel(this.consumerTag);
    logger.info('messenger consumption cancelled.');
  },
  async sendUrlsToCrawler(urls) {
    logger.info('serializing urls to send to crawlers...');
    const message = Buffer.from(JSON.stringify(urls));
    logger.info('message serialized.');
    logger.info('generating message unique correlationId...');
    const correlationId = uuid.generate();
    logger.info({ correlationId }, 'message unique correlationId generated.');
    logger.info('sending message to queue...');
    this.channel.sendToQueue(queues.send.name, message, {
      contentType: 'application/json',
      correlationId,
      replyTo: queues.receive.name,
      persistent: queues.send.durable,
    });
    logger.info('message was sent to queue...');
    return correlationId;
  },
};

module.exports = Messenger;
