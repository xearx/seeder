/* eslint-disable global-require */
const Async = require('async');
const bus = require('./bus');

function startup() {
  return Async.waterfall([
    async function configureDotenv() {
      require('dotenv').config();
    },
    async function adjustConfig() {
      require('./config');
    },
    async function initializeLogger() {
      require('./logger');
    },
    async function registerBusListeners() {
      require('./repository').registerListeners(bus);
      require('./seeder').registerListeners(bus);
    },
    async function initializeSeeder() {
      const messenger = require('./messenger');
      const repository = require('./repository');
      return require('./seeder').init({
        bus,
        messenger,
        repository,
      });
    },
    async function connectDatabase() {
      await require('./repository/db').connect();
    },
    async function connectFiltererService() {
      await require('./grpc/clients/filterer').connect();
    },
    async function connectMessenger() {
      await require('./messenger').init({ bus });
    },
    async function ensureCrawlStat() {
      return require('./repository').ensureStat();
    },
    async function startHttpServer() {
      return require('./http').startServer();
    },
  ]);
}

function teardown() {
  return Promise.all([
    require('./http').stopServer(),
    require('./repository/db').disconnect(),
    require('./messenger').disconnect(),
  ]);
}

module.exports = {
  startup,
  teardown,
};
