const path = require('path');
const grpc = require('grpc');
const protoLoader = require('@grpc/proto-loader');
const config = require('../config');

module.exports = function resolveProtoDefnitions(...protoDefinitions) {
  return protoDefinitions.map((protoDefinition) => {
    const PROTO_PATH = path.resolve(config.grpc.proto.relative_path, `${protoDefinition}.proto`);

    const packageDefinition = protoLoader.loadSync(
      PROTO_PATH,
      {
        keepCase: true,
        longs: String,
        enums: String,
        defaults: true,
        oneofs: true,
      },
    );

    return { protoDefinition, packageDefinition: grpc.loadPackageDefinition(packageDefinition) };
  })
    .reduce((acc, curr) => ({
      ...acc,
      [curr.protoDefinition]: curr.packageDefinition,
    }), {});
};
