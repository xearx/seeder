const { DateTime, Duration } = require('luxon');
const grpc = require('grpc');
const resolveProtoDefnitions = require('../proto_resolver');
const config = require('../../config');
const logger = require('../../logger');

const { filterer } = resolveProtoDefnitions('filterer');

const { host, port } = config.grpc.clients.filterer;

const Client = new filterer.Filterer(`${host}:${port}`, grpc.credentials.createInsecure());

function connect() {
  logger.trace({ host, port }, 'connecting to Filterer service at:');
  const deadline = Duration.fromMillis(config.grpc.clients.connectionTimeoutSeconds * 1_000);
  return new Promise((resolve, reject) => {
    const deadlineTime = DateTime.utc().plus(deadline).toJSDate();
    Client.waitForReady(deadlineTime, (error) => {
      logger.trace('connection timeout reached.');
      if (error) {
        logger.error(error, 'could not connect to Filterer service.');
        reject(error);
      } else {
        logger.info('connected to Filterer service.');
        resolve(Client);
      }
    });
  });
}

function disconnect() {
  return Client.close();
}

function filterUrls(urls) {
  return new Promise((resolve, reject) => {
    Client.FilterURLs({ urls }, (error, reply) => {
      if (error) {
        reject(error);
        return;
      }
      resolve(reply.urls);
    });
  });
}

module.exports = {
  connect,
  disconnect,
  filterUrls,
};
