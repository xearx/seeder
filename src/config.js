const config = require('config');

module.exports = {
  server: {
    port: parseInt(config.get('server.port'), 10),
    host: config.get('server.host').toString(),
  },
  database: {
    host: config.get('database.host').toString(),
    port: parseInt(config.get('database.port'), 10),
    name: config.get('database.name').toString(),
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    useAuth: Boolean(config.get('database.use_auth')),
  },
  repository: {
    batchSize: parseInt(config.get('repository.batch_size'), 10),
  },
  logger: {
    level: config.get('logger.level').toString(),
  },
  messaging: {
    rabbitmq: {
      host: config.get('messaging.rabbitmq.host').toString(),
      port: parseInt(config.get('messaging.rabbitmq.port'), 10),
      queues: {
        send: {
          name: config.get('messaging.rabbitmq.queues.send.name').toString(),
          durable: Boolean(config.get('messaging.rabbitmq.queues.send.durable')),
        },
        receive: {
          name: config.get('messaging.rabbitmq.queues.receive.name').toString(),
          durable: Boolean(config.get('messaging.rabbitmq.queues.receive.durable')),
        },
      },
    },
  },
  seeder: {
    buffer: {
      flushIntervalInSeconds: parseInt(config.get('seeder.buffer.flush_interval_in_seconds'), 10),
    },
    requestBucketSize: parseInt(config.get('seeder.request_bucket_size'), 10),
  },
  grpc: {
    proto: {
      relative_path: config.get('grpc.proto.relative_path'),
    },
    clients: {
      connectionTimeoutSeconds: parseInt(config.get('grpc.clients.connection_timeout_seconds'), 10),
      filterer: {
        host: config.get('grpc.clients.filterer.host'),
        port: parseInt(config.get('grpc.clients.filterer.port'), 10),
      },
    },
  },
};
