const logger = require('./logger');
const { startup, teardown } = require('./startup');

async function main() {
  try {
    await startup();
  } catch (error) {
    logger.error(error, 'error');
    logger.info('shutting down...');
    await teardown();
  }
}

if (require.main === module) {
  main();
}
