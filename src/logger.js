const pino = require('pino');
const config = require('./config');

module.exports = pino({
  level: config.logger.level,
  prettyPrint: {
    colorize: true,
    translateTime: true,
  },
});
